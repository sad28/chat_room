var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
const { uniqueNamesGenerator, starWars } = require('unique-names-generator'); 

var list = [];

app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){

  //console.log('user connected');
  var nickname = uniqueNamesGenerator({ dictionaries: [starWars] }); 
  while(list.filter(user => user.nickname === nickname).length > 0){
    nickname = uniqueNamesGenerator({ dictionaries: [starWars] });
  }
  
  var user = {"nickname" : nickname, "id": socket.id} 
  list.push(user);

  io.emit('list', list);
  io.emit('connected', user);

  socket.on('disconnect', function(){

    //console.log('user disconnected'); 
    var user_index = list.indexOf(user);

    //remove user from the list array
    list.splice(user_index, 1);

    io.emit('list', list);
    io.emit('disconnected', user);
  });

  socket.on('chat message', function(msg){
    //console.log('message: ' + msg); 
    var chat_message = {"nickname" : nickname, "content": msg} 
    socket.broadcast.emit('chat message', chat_message); 
  });

  socket.on('typing', function(){
    //console.log('message: ' + msg); 
    var typing_message = {"nickname" : nickname, "content": "is typing..."} 
    socket.broadcast.emit('typing', typing_message); 
  });

  socket.on('priv chat message', function(msg, to_user_id){ 
    //console.log('message: ' + msg); 
    var priv_message = {"nickname" : nickname, "content": msg + " (mensaje privado)"}; 
    io.to(to_user_id).emit('chat message', priv_message);
  });

});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
