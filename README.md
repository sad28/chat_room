# chat_room

clone repository and then load dependencies by executing
```
npm install
```
to execute code run 
```
node index.js
```
then go to 
``` 
http://localhost:3000/ 
```
open multiple tabs to see the chat behaviour

# Tareas a realizar

1. Distribuir un mensaje a todos los usuarios conectados cuando alguien se
conecta o se desconecta. 

    - Mediante la llamada a la función emit, perteneciente a io, conseguimos que se avise al resto de personas conectadas quién se ha conectado en ese mismo momento. Dicho usuario que se acaba de conectar se añadirá al array de usuarios conectados que se ha indicado mediante la variable "list".

    - El chat avisa cada vez que alguien se conecta mediante un mensaje del sistema con estilo italic que se muestra en la pantalla. 
    
    Comprobar que usuarios se han desconectado del chat.

    - A diferencia de las conexiones de los usuarios, las desconexiones se realizarán mediante el socket que se ha creado inicialmente. Se buscará a lo largo del array "list" el usuario que ha querido desconectarse, una vez encontrado, se borrará del array. Esto se ha realizado mediante la función "splice".

    - De la misma forma que antes el chat avisará cuando alguien se desconecte.


2. Añadir soporte a nicknames

    - Se ha utilizado el atributo id de cada socket cuando estos se crean al connectarse un cliente y con la ayuda de un modulo adicional "unique-names-generator" se han creado nombres de personajes de star wars para identificar de forma sencilla a modo de alias a los usuarios en lugar de usar el id. (Los alias se comprueban tmabién para que sean únicos)  


3. No enviar el mensaje al usuario que ha enviado su mensaje.

    - Mediante la función "broadcast.emit" sobre el socket creado, conseguimos que el mensaje sea enviado a todos los usuarios menos el usuario que lo ha escrito. En el chat si se puede ver lo que un mismo usuario ha escrito pero por que ese valor lo leemos desde el propio cliente, es decir el contenido del mensaje no pasa por el servidor.   


4. Mensaje a los usuarios cuando alguien escribe.

    - Se ha añadido la funcionalidad que cuando un usuario esté escribiendo aparecerá en le chat un mensaje del sistema con “{usuario} está escribiendo”.Esto se realiza con un listener sobre el tipo de evento keypress y se distribuye un mensaje cada vez que se pulsa una tecla, solo se muestra un mensaje a la vez.
    Esta tarea también se realiza mediante la función "broadcast.emit" sobre el socket creado.   


5. OPCIONAL Mostrar la lista de usuarios conectados

    - En el chat se ha añadido la funcionalidad donde aparecen los usuarios conectados, se utiliza como base el array de usuarios descrito en el punto 1, con la diferencia en que en este listado se indicará que nombre se le ha asignado al usuario.  


6. OPCIONAL añadir soporte para mensajes privados 

    - Haciendo uso de la lista de usuarios de la derecha se pueden enviar mensajes privados para ello hay que hacer click sobre el usuario al que se quiere enviar el mensaje privado para que se quede seleccionado (no se puede enviar un mensaje privado a uno mismo). El mensaje se enviará solo a ese usuario y aparecera entre paréntesis que es privado. 
    
    - Si se quiere salir del modo privado solo hay que volver a hacer click sobre el usuario para deselecionar lo y volveremos a enviar mensaje publicos. Para cambiar de usario haremos lo mismo click en el usario y se deseleccionará el anterior usuario debido a que solo se pueden enviar mensajes privados a un destinatario.   

# Link al repositorio
https://gitlab.com/sad28/chat_room

## Useful Links  
https://socket.io/docs/v3/broadcasting-events/  
https://socket.io/get-started/chat  
https://socket.io/docs/v3/server-socket-instance/  

https://stackoverflow.com/questions/11484418/socket-io-broadcast-to-certain-users  
https://www.section.io/engineering-education/keyboard-events-in-javascript/  
